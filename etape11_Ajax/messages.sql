--
-- Base de données :  `demosw`
--
CREATE DATABASE IF NOT EXISTS `demosw` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `demosw`;

-- --------------------------------------------------------

--
-- Structure de la table `messages`
--

DROP TABLE IF EXISTS `messages`;

CREATE TABLE `messages` (
  `msg.id` int(11) NOT NULL AUTO_INCREMENT,
  `msg.timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `msg.nomprenom` varchar(32) NOT NULL,
  `msg.telephone` char(9) DEFAULT NULL,
  `msg.email` varchar(32) DEFAULT NULL,
  `msg.texte` text NOT NULL,
  PRIMARY KEY (`msg.id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `messages`
--

INSERT INTO `messages` (`msg.id`, `msg.timestamp`, `msg.nomprenom`, `msg.telephone`, `msg.email`, `msg.texte`) VALUES(1, '2017-09-11 05:24:16', 'Jean PeuxPlus', '123456789', 'jpp@ninetythree.com', 'Comment bootstrapper mon pied ?');
INSERT INTO `messages` (`msg.id`, `msg.timestamp`, `msg.nomprenom`, `msg.telephone`, `msg.email`, `msg.texte`) VALUES(2, '2017-09-13 05:26:29', 'Gui Root', '320032003', NULL, 'Peut-on sécuriser un site PHP ?');
INSERT INTO `messages` (`msg.id`, `msg.timestamp`, `msg.nomprenom`, `msg.telephone`, `msg.email`, `msg.texte`) VALUES(4, '2017-09-13 07:31:05', 'Troy Zieme', NULL, 'troy@zieme.name', 'L\'accessibilité, c\'est ARIA ?');
INSERT INTO `messages` (`msg.id`, `msg.timestamp`, `msg.nomprenom`, `msg.telephone`, `msg.email`, `msg.texte`) VALUES(5, '2017-09-13 12:36:19', 'Katre Car', '123456789', NULL, 'mais pourquoi donc autant de caractères ?');
INSERT INTO `messages` (`msg.id`, `msg.timestamp`, `msg.nomprenom`, `msg.telephone`, `msg.email`, `msg.texte`) VALUES(7, '2017-09-13 13:42:27', 'Sans retour', NULL, NULL, 'Je ne fournis aucun contact.');
